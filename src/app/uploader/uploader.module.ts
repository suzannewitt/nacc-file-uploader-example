import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploaderComponent } from './uploader.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';

import {
  FwLayoutsModule,
  FwCardModule,
  FwButtonModule,
  FwAlertModule,
  FwIconModule,
} from '@flywheel-io/vision';



@NgModule({
  declarations: [
    UploaderComponent
  ],
  imports: [
    CommonModule,
    FwLayoutsModule,
    FwCardModule,
    FwButtonModule,
    FwAlertModule,
    MatProgressSpinnerModule,
    MatTableModule,
    FwIconModule,
  ],
  exports: [ UploaderComponent ]
})
export class UploaderModule { }
