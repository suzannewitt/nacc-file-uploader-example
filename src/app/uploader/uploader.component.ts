import { AfterRenderPhase, Component, ElementRef, ViewChild, afterNextRender } from '@angular/core';
import { ExtensionScope, FileUploadEvent, initExtension, Extension } from '@flywheel-io/extension';
import { Observable, forkJoin, map, tap, EMPTY } from 'rxjs';


class FileEntry {
    public file: File
    public error: string
    public valid: boolean
    public uploaded: boolean

    constructor(file: File){
        this.file = file;
        this.valid = file.name.match(regex) !== null;
        this.uploaded = false;
    }
}

function isSameUrl(href1: URL | string, href2: URL | string) {
    const url1 = new URL(href1, window.location.href);
    const url2 = new URL(href2, window.location.href);
    url1.hash = '';
    url2.hash = '';
    return String(url1) === String(url2);
}

function isDevelopmentMode() {
    return window.self === window.top && window.location.hostname === 'localhost';
}


const regex = /^.*-([a-z]+v[0-9])\.csv$/
declare let eta: any;

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrl: './uploader.component.scss'
})
export class UploaderComponent {

    @ViewChild("fileInput")
    fileInput: ElementRef;

    extension: any;

    files: Array<FileEntry> = [];

    errString: string = "";
    successString: string = "";
    loading: boolean = false;
    dragging = false;
    displayedColumns: string[] = ['name', 'size', 'uploaded', 'error'];


    calculateFileSize(size: number) {
        // Approximate to the closest prefixed unit
        const units = [
            "B",
            "KiB",
            "MiB",
            "GiB",
            "TiB",
            "PiB",
            "EiB",
            "ZiB",
            "YiB",
        ];
        const exponent = Math.min(
            Math.floor(Math.log(size) / Math.log(1024)),
            units.length - 1,
        );
        const approx = size / 1024 ** exponent;
        const output =
        exponent === 0
            ? `${size} bytes`
            : `${approx.toFixed(3)} ${
                units[exponent]
            } (${size} bytes)`;
        return output;
    }

    redirect() {
        const { redirectUrl, newTab } = this.extension.params;
        console.log(redirectUrl);
        if (!redirectUrl) {
            this.errString = 'Missing parameter: redirectUrl';
            setTimeout(() => {
                this.errString = "";
            }, 5000);
            return;
        }
  
        const { container, file, params, user } = this.extension;
        const site = {
            url: new URL(document.referrer || window.location.href, window.location.href),
        };
        try {
            // I have no idea wha this useWith does, but it was in Jody's code...
            const renderer = new eta.Eta({ useWith: !/\Wit[.[]/.test(redirectUrl) });
            console.log(container, file, params, site, user);
            const uriHref = renderer.renderString(
                redirectUrl,
                { container, file, params, site, user },
            );
  
            if (uriHref.startsWith('http')) {
                if (!isSameUrl(uriHref, window.location.href)) {
                    if (newTab) {
                        // open URLs in a new tab
                        window.open(uriHref, '_blank', 'noopener');
                    } else {
                        if (window.top){
                            window.top.location.href = uriHref;
                        }
                    }
                }
            } else {
                // all other protocols
                window.location.href = uriHref;
            }
  
            this.extension.close();
        } catch (error) {
            console.error(error);
            this.errString = 'The URL could not be built from the template with the provided data';
            setTimeout(() => {
                this.errString = "";
            }, 5000);
        }
    }
  
    processFiles(files: Array<File>): Array<FileEntry> {
        return files.map(file => new FileEntry(file));
    }

    ngOnInit() {
        initExtension({
            scope: ExtensionScope.ReadWrite,
            validateOrigin: origin => origin.endsWith('flywheel.io')
        }).then(extension => {
            this.extension = extension;
        })

    }

    constructor() {
        afterNextRender(() => {
          const elem = this.fileInput.nativeElement;
        }, {phase: AfterRenderPhase.Write});
        console.log(eta);
      }

    drop(event: any){
        console.log("here")
        event.preventDefault();
        event.stopPropagation();
        console.log(event)
    }

    onDrop(event: DragEvent) {
        event.preventDefault();
        event.stopPropagation();
        this.dragging = false;
        if (event.dataTransfer) {
            console.log(event.dataTransfer.files);
            this.files = this.processFiles(Array.from(event.dataTransfer.files));
            console.log(this.files)
        }
    }

    clearError(): void{
        this.errString = "";
    }

    clearSuccess(){
        this.successString = "";
    }

    clearFiles(){
        this.files = [];
    }

    onDragOver(event: DragEvent) {
        event.preventDefault();
        event.stopPropagation();
        this.dragging = true;
    }
    onDragLeave(event: DragEvent){
        event.preventDefault();
        event.stopPropagation();

        this.dragging = false;

    }
    handleClick(event: MouseEvent){
        event.preventDefault();
        event.stopPropagation();
        this.fileInput.nativeElement.click();
    }

    onChanges(event: Event){
        if(event.target) {
            const files = (event.target as HTMLInputElement).files;
            if(files) {
                this.files = this.processFiles(Array.from(files));
            }

        }
    }

    uploadFiles() {
        if(this.files.length === 0){
            this.errString = "No files selected";
            setTimeout(() => {
                this.errString = "";
            }, 5000);
            return;
        }
        if(!isDevelopmentMode() && !this.extension) {
            this.errString = "Flywheel extension not initialized";
            setTimeout(() => {
                this.errString = "";
            }, 5000);
            return;
        }
        this.loading = true;

        const observables = this.files.map((file, i)  => {
            if (!file.valid){
                this.files[i].error = `Invalid filename, needs to match <code id='code'>${regex}</code>, example <code id='code'>mydata-udsv4.csv</code>`;
                return [];
            }
            let options = {
                filename: file.file.name,
            }
            let obs: Observable<any>;
            if (isDevelopmentMode()){
                obs = EMPTY;
            } else {
                obs = this.extension.uploadFile(this.extension.container, file.file, options).pipe(
                    tap((evt: FileUploadEvent) => {
                        if (evt.error){
                            this.files[i].error = evt.error.message;
                        }
                    })
                );
            }

            return obs;

        });
        forkJoin(...observables).subscribe({
            next: (event) => {
                console.log(event)
            },
            complete: () => {
                var uploaded = 0;
                this.files.forEach((file, i) => {
                    if (!file.error){
                        this.files[i].uploaded = true;
                        uploaded++;
                    }
                });
                this.loading = false;
                if(uploaded === this.files.length){
                    this.successString = "All files uploaded successfully";
                    setTimeout(() => {
                        this.successString = "";
                    }, 5000);
                    this.redirect();
                } else {
                    this.errString = `Uploaded ${uploaded} out of ${this.files.length} files.`;
                    setTimeout(() => {
                        this.errString = "";
                    }, 5000);

                }
            }
        });
    }

}
