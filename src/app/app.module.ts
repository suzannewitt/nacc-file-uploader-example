import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'

import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async'; // Adjust the path based on your actual file structure
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatCommonModule } from '@angular/material/core';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

import { UploaderModule } from './uploader/uploader.module';





import {
  FwCardModule,
  FwAvatarModule,
  FwButtonModule,
  FwIconModule,
  FwPopoverModule,
  FwSectionHeadingModule,
  FwLayoutsModule,
  FwAlertModule
} from '@flywheel-io/vision';




@NgModule({
  declarations: [
    AppComponent,


    //SummaryStatsComponent
   // CsvTableComponent
    // other components,
    
  ],
  imports: [
    CommonModule,
    MatTableModule,
    FormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    FwCardModule,
    FwAvatarModule,
    FwButtonModule,
    FwIconModule,
    FwPopoverModule,
    FwSectionHeadingModule,
    MatInputModule,
    MatCommonModule,
    MatSlideToggleModule,
    FwLayoutsModule,
    UploaderModule,
    FwAlertModule,
    
    
  ],
  providers:
  [
    provideAnimationsAsync(),

  ],
  bootstrap: [AppComponent] // This line marks AppComponent for bootstrapping

// bootstrap: [AppComponent]
  // other module configurations
})
export class AppModule {}
