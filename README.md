# NaccFileUploader

Flywheel viewer extension app that will open a dialog for uploading CSV files, will redirect to the desired location when closed.

## Registration

Register this application for files/containers at the site or a project Application page. Define the `uriTemplate` in "Application options":

```json
{
  "redirectUrl": "https://flywheel.io/#/projects/<%= container._id %>",
  "newTab": false
}
```

## Template Syntax

The `redirectUrl` string is processed with the [Eta](https://eta.js.org/docs) template engine. Evaluate JavaScript and variables using `<%= obj.prop %>` syntax. The template scope contains these properties in addition to standard globals:

* [container](https://cdn.flywheel.io/docs/extension/latest/interfaces/container.html)
* [file](https://cdn.flywheel.io/docs/extension/latest/interfaces/file.html)
* params - application options defined with the registration
* site - info about the Flywheel site hosting this application
  * url - URL object referring to the site where the application was launched
* [user](https://cdn.flywheel.io/docs/extension/latest/interfaces/user.html)


## Development

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
